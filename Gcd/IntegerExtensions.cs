﻿using System;
using System.Diagnostics;

#pragma warning disable 

namespace Gcd
{
    /// <summary>
    /// Provide methods with integers.
    /// </summary>
    public static class IntegerExtensions
    {
        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b)
        {
            if (a == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a));
            }

            if (b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(b));
            }

            if (a == 0 && b == 0)
            {
                throw new ArgumentException(string.Empty, nameof(a));
            }
            else if (a == 0) return Math.Abs(b);
            else if (b == 0) return Math.Abs(a);


            int f = a;
            int s = b;
            int r = 1;
            while (r != 0)
            {
                r = f % s;
                if (r == 0) break;
                f = s;
                s = r;
            }

            return Math.Abs(s);

        }

        /// <summary>
        /// Calculates GCD of three integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b, int c)
        {
            int gcd;
            if(a!=0 || b!=0)
            {
                gcd = GetGcdByEuclidean(a, b);
                return GetGcdByEuclidean(gcd, c);
            }
            gcd = GetGcdByEuclidean(a, c);
            return GetGcdByEuclidean(gcd, b);
        }

        /// <summary>
        /// Calculates the GCD of integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(int a, int b, params int[] other)
        {
            int gcd;
            if (a != 0 || b != 0)
            {
                gcd = GetGcdByEuclidean(a, b);
                for (int i = 0; i <other.Length; i++)
                {
                    gcd = GetGcdByEuclidean(gcd, other[i]);
                }

                return gcd;
            }
            else
            {
                int first = -1;
                for (int i = 0; i < other.Length; i++)
                {
                    if(other[i] != 0)
                    {
                        first = i;
                        break;
                    }
                }
                if(first == -1)
                {
                    throw new ArgumentException(string.Empty, nameof(a));
                }
                else
                {
                    gcd = other[first];
                    for (int i = first + 1; i < other.Length; i++)
                    {
                        gcd = GetGcdByEuclidean(gcd, other[i]);
                    }
                    return gcd;
                }
            }
        }

        /// <summary>
        /// Calculates GCD of two integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByStein(int a, int b)
        {
            if (a == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a));
            }

            if (b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(b));
            }

            if (a == 0 && b == 0)
            {
                throw new ArgumentException(string.Empty, nameof(a));
            }
            else if (a == 0) return Math.Abs(b);
            else if (b == 0) return Math.Abs(a);

            a = Math.Abs(a);
            b = Math.Abs(b);
            int count = 0;
            while(a!=b)
            {
                if (a % 2 == 0 && b % 2 == 0)
                {
                    a /= 2;
                    b /= 2;
                    count++;
                }
                else if (a % 2 == 0)
                {
                    a /= 2;
                }
                else if (b % 2 == 0)
                {
                    b /= 2;
                }
                else
                {
                    int c = Math.Abs(a - b);
                    a = Math.Min(a, b);
                    b = c;
                }
            }
            int ans = a * (int)Math.Pow(2, count);
            return ans;

        }

        /// <summary>
        /// Calculates GCD of three integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(int a, int b, int c)
        {
            int gcd;
            if (a != 0 || b != 0)
            {
                gcd = GetGcdByStein(a, b);
                return GetGcdByStein(gcd, c);
            }
            gcd = GetGcdByStein(a, c);
            return GetGcdByStein(gcd, b);
        }

        /// <summary>
        /// Calculates the GCD of integers [-int.MaxValue;int.MaxValue] by the Stein algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(int a, int b, params int[] other)
        {
            int gcd;
            if (a != 0 || b != 0)
            {
                gcd = GetGcdByStein(a, b);
                for (int i = 0; i < other.Length; i++)
                {
                    gcd = GetGcdByStein(gcd, other[i]);
                }

                return gcd;
            }
            else
            {
                int first = -1;
                for (int i = 0; i < other.Length; i++)
                {
                    if (other[i] != 0)
                    {
                        first = i;
                        break;
                    }
                }
                if (first == -1)
                {
                    throw new ArgumentException(string.Empty, nameof(a));
                }
                else
                {
                    gcd = other[first];
                    for (int i = first + 1; i < other.Length; i++)
                    {
                        gcd = GetGcdByStein(gcd, other[i]);
                    }
                    return gcd;
                }
            }
        }

        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b)
        {
            Stopwatch stop = new Stopwatch();
            stop.Start();
            int gcd = GetGcdByEuclidean(a, b);
            stop.Stop();         
            elapsedTicks = stop.ElapsedTicks;
            return gcd;
        }

        /// <summary>
        /// Calculates GCD of three integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, int c)
        {
            Stopwatch stop = new Stopwatch();
            stop.Start();
            int gcd = GetGcdByEuclidean(a, b, c);
            stop.Stop();
            elapsedTicks = stop.ElapsedTicks;
            return gcd;
        }

        /// <summary>
        /// Calculates the GCD of integers from [-int.MaxValue;int.MaxValue] by the Euclidean algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in Ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, params int[] other)
        {
            Stopwatch stop = new Stopwatch();
            stop.Start();
            int gcd = GetGcdByEuclidean(a, b, other);
            stop.Stop();
            elapsedTicks = stop.ElapsedTicks;
            return gcd;
        }

        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue] by the Stein algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int GetGcdByStein(out long elapsedTicks, int a, int b)
        {
            Stopwatch stop = new Stopwatch();
            stop.Start();
            int gcd = GetGcdByStein(a, b);
            stop.Stop();
            elapsedTicks = stop.ElapsedTicks;
            return gcd;
        }

        /// <summary>
        /// Calculates GCD of three integers from [-int.MaxValue;int.MaxValue] by the Stein algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="c">Third integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(out long elapsedTicks, int a, int b, int c)
        {
            Stopwatch stop = new Stopwatch();
            stop.Start();
            int gcd = GetGcdByStein(a, b, c);
            stop.Stop();
            elapsedTicks = stop.ElapsedTicks;
            return gcd;
        }

        /// <summary>
        /// Calculates the GCD of integers from [-int.MaxValue;int.MaxValue] by the Stein algorithm with elapsed time.
        /// </summary>
        /// <param name="elapsedTicks">Method execution time in Ticks.</param>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <param name="other">Other integers.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or more numbers are int.MinValue.</exception>
        public static int GetGcdByStein(out long elapsedTicks, int a, int b, params int[] other)
        {
            Stopwatch stop = new Stopwatch();
            stop.Start();
            int gcd = GetGcdByStein(a, b, other);
            stop.Stop();
            elapsedTicks = stop.ElapsedTicks;
            return gcd;
        }
    }
}
